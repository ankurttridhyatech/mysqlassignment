1) /* Create database using query */ 

Create database ;

2) /* Create table using query (create 2 table members and moviews) */

use assi_1;

CREATE TABLE members (
    
    membership_number int(4) primary key,
    full_names varchar(50),
    gender varchar(20),
    date_of_birth date,
    physical_address varchar(200),
    postal_address varchar(200),
    contact_number bigint(10),
    email varchar(30)
);

use assi_1;

CREATE TABLE movies (
    
    movie_id int(4) primary key,
    title varchar(50),
    director varchar(20),
    year_released year,
    category_id  int(3)
);

3) /* Create different-different Select query with DISTINCT,*,FROM, WHERE,GROUP BY,HAVING and HAVING */  

SELECT DISTINCT year_released FROM movies;
    
SELECT * FROM movies WHERE year_released IN (2007);

SELECT full_names,date_of_birth FROM members;

SELECT * FROM members WHERE gender='Female';

SELECT title , year_released FROM movies GROUP BY year_released;

SELECT title , year_released FROM movies GROUP BY movie_id HAVING movie_id > 4;


4) /* Create query using WHERE Clause(IN, OR, AND, Not IN,Equal To,Not Equal To, Greater than, less than ) */  

SELECT * FROM movies WHERE movie_id IN ('1','3','5');

SELECT * FROM movies WHERE year_released='2007' OR year_released='2005';

SELECT * FROM movies WHERE year_released='2007' AND category_id='8';

SELECT * FROM movies WHERE movie_id NOT IN ('1','3','5');

SELECT * FROM movies WHERE movie_id = 2;

SELECT * FROM movies WHERE movie_id != 6;

SELECT * FROM movies WHERE movie_id > 9;

SELECT * FROM movies WHERE movie_id < 4;



5) /* Create insert query */

INSERT INTO `members`(`membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email`) VALUES (1,'Janet Jones','Female','1980-07-21','First street plot No 4','PrivateBag',0759253542,'janetjones@yahoo.cm')

INSERT INTO `members`(`membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email`) VALUES (2,'Janet Smith Jones','Female','1980-06-23','Melrose123',NULL,NULL,'jj@fstreet.com')

INSERT INTO `members`(`membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email`) VALUES (3,'Robert Phil','Male','1989-07-12','3rd Street34',NULL,12345,'rm@tstreet.com')

INSERT INTO `members`(`membership_number`, `full_names`, `gender`, `date_of_birth`, `physical_address`, `postal_address`, `contact_number`, `email`) VALUES (4,'Gloria Williams','Female','1984-02-14','2nd Street23',NULL,NULL,NULL)


INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (1,'Pirates of the Caribean4','Rob MArshall',2011,1)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (2,'Forgetting Sarah Marshal','Nicholas Stoller',2008,2)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (3,'X-Men',NULL,2008,NULL)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (4,'Code Name Black','Edgar Jimz',2010,NULL)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (5,'Daddys Little Girls',NULL,2007,8)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (6,'Angels and Demos',NULL,2007,6)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (7,'Davinvi Code',NULL,2007,6)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (9,'Honey mooners','John Schultz',2005,8)

INSERT INTO `movies`(`movie_id`, `title`, `director`, `year_released`, `category_id`) VALUES (16,'67%Guilty',NULL,2012,NULL)

6) /* Get record form members table using select query */

SELECT * from members;

7) /* Getting only the full_names, gender, physical_address and email fields only from memeber table */

SELECT full_names, gender, physical_address,email fields from members;

8) /* Get a member's personal details from members table given the membership number 1 */

SELECT * FROM members WHERE membership_number='1'

9) /* Get a list of all the movies in category 2 that were released in 2008 */ 

SELECT title FROM movies WHERE category_id='2' AND year_released='2008';

10) /* Gets all the movies in either category 1 or category 2 */ 

SELECT * FROM movies WHERE  category_id='1' Or category_id='2' ;

11) /* Gets rows where membership_number is either 1 , 2 or 3 */ 

SELECT * FROM members WHERE membership_number='1' OR membership_number='2' Or membership_number='3';

12) /* Gets all the female members from the members table using the equal to comparison operator. */	

SELECT full_names FROM members WHERE gender = 'female';

13) /* Gets all the payments that are greater than 2,000 from the payments table */ 

SELECT * FROM payments WHERE payment > 2000;

